import React from "react";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import CountMoviment from "./src/countMoviment";
import HomeScreen from "./src/homeScreen";
import CarouselImages from "./src/caroulselImages";

const Stack = createStackNavigator();

export default function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="App">
        <Stack.Screen name="Home" component={HomeScreen} options={{title:'Página Inicial'}} />
        <Stack.Screen name="CountMoviment" component={CountMoviment} options={{title:'Contador de movimento'}} />
        <Stack.Screen name="CaroulselImages" component={CarouselImages} options={{title:'Carrossel de Imagens'}} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}


