import React from "react";
import { View, Button } from "react-native";
import { useNavigation } from "@react-navigation/native";


const HomeScreen = ()=>{
    const navigation = useNavigation();

    const handleCarouselImages  = () => {
        navigation.navigate('CaroulselImages');
    }

    const handleCountMoviment = () => {
        navigation.navigate('CountMoviment');
    }


    return(
        <View>
            <Button title="Contador por Gesto" onPress={handleCountMoviment}/>
            <Button title="Carrossel de Imagens" onPress={handleCarouselImages}/>
        </View>
    )
};
export default HomeScreen;