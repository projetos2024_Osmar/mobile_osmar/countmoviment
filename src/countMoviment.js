import React , {useRef, useState, useEffect} from "react";
import { View, Text, Dimensions, PanResponder, Button } from "react-native";

const CountMoviment = () => {
    const [count, setCount] = useState(0);
    const screenWeight = Dimensions.get("window").height;
    const gestureThreshold = screenWeight * 0.25;

    const panResponder = useRef(
        PanResponder.create({
            onMoveShouldSetPanResponder: ()=>true,
            onPanResponderMove:(event,gestureState)=>{},
            onPanResponderRelease:(event, gestureState)=>{
                if(gestureState.dy < -gestureThreshold){
                    setCount((prevCount)=>prevCount+1)
                }
                else if(gestureState.dy > -gestureThreshold){
                    setCount((prevCount)=>prevCount-1)
                }
            }
        })
    ).current;

    return(
        <View {...panResponder.panHandlers} style={{flex:1, alignItems:'center', justifyContent:'center'}}>
            <Text>Valor do contador {count}</Text>
        </View>
    );
}
export default CountMoviment;